# TrustedTS 

* Es un paquete que permite aplicar un sellado de tiempo a un archivo , tambien permite la verificación del sello de tiempo.

**INSTALL**

` composer require qerana/trustedts`

**REQUISITOS**

- openssl2
- curl

**EJEMPLOS**

Despues de ejecutar con composer,copiar los archivos/carpetas de la carpeta examples a la raiz.

`cp -R vendor/qerana/trustedts/examples/* .`

En la carpeta data/ se guardan las peticiones de sellado.
En data/tsa se guardan los archivos .pem y crt del TSA (solo necesario para la verficacion).

**Para sellar un archivo:**

Ver el archivo stamp.php y ejecutar por CLI

`php stamp.php`

**Para verificar un sello:**

 Ver el archivo verify.php  y ejecutar por CLI

`php verify.php`

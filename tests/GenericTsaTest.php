<?php
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Tests;

use PHPUnit\Framework\TestCase;
use Qerana\Trustedts\tsa\GenericTsa;

/**
 * Description of GenericTsaTest
 *
 * @author diemarc
 */
class GenericTsaTest extends TestCase
{
    protected $GenericTsa;

    /**
     * un tsa valido
     * @var type string
     */
    protected $valid_tsa = 'https://freetsa.org/tsr';

    /**
     * Una url no valida para un TSA
     * @var type string
     */
    protected $invalid_tsa = 'https://freetsa.py';

//    public function setUp(): void
//    {
//        $this->GenericTsa = new GenericTsa($this->tsa_server);
//    }

    /**
     * Comprueba si el construtor arroja una exepcion si se pasa
     * una url invalida
     */
    public function testConstructInvalidUrl()
    {

        $this->expectException(\InvalidArgumentException::class);
        new GenericTsa('invalid_url');
    }

    /**
     * Testea si asigna correctamente la url del TSA server.
     */
    public function testConstructValidUrl()
    {

        $Generic = new GenericTsa($this->valid_tsa);
        $this->assertEquals($this->valid_tsa, $Generic->getTsaUrl());
    }

    /**
     * Comprueba si pasando un tsa_server invalido devuelve una excepcion
     */
    public function testIfVerifyTsaThrowExceptionInvalidTSA()
    {
        $this->expectException(\RuntimeException::class);
        $Tsa = new GenericTsa($this->invalid_tsa);
        $Tsa->verifyTsa();
    }

    /**
     * Verifica si verificando una url valida lo asigna corretamente
     */
    public function testIfVerifyTsaPassValidUrl()
    {

        $Generic = new GenericTsa($this->valid_tsa);
        $Generic->verifyTsa();
        $this->assertEquals($this->valid_tsa, $Generic->getTsaUrl());
    }

    /**
     * Comprueba el seteo de credenciales
     */
    public function testSetCredentials()
    {

        $user = 'username';
        $pass = 'password';

        $Tsa = new GenericTsa($this->valid_tsa);
        $Tsa->setCredentials($user, $pass);

        $this->assertEquals($Tsa->getUser(), $user);
        $this->assertEquals($Tsa->getPass(), $pass);
        $this->assertEquals($Tsa->getPass(), $pass);

        return $Tsa;
    }

    /**
     * Comprueba que al setear las credenciales el atributo auth
     * se haya puesto a true
     */
    public function testCredentialsIfAuthReturnsTrue()
    {

        $Tsa = $this->testSetCredentials();
        $this->assertEquals(true, $Tsa->getAuth());
    }

    /**
     * Verifica que la carpeta de datos se ha creado
     * correctamente
     */
    public function testTsaFolderCreation()
    {


        $folder = 'var/data/tsa';
        $Tsa    = new GenericTsa($this->valid_tsa);
        $Tsa->setTsaDataFolder($folder);
        $this->assertDirectoryExists($folder);

        return $Tsa;
    }

    /**
     * Comprueba que el archivo a sellar existe.
     */
    public function testExceptionFile()
    {

        $this->expectException(\InvalidArgumentException::class);
        $example = 'var/data/doc11.pdf';
        $Tsa     = $this->testTsaFolderCreation();
        $Tsa->setFile($example);
    }

    /**
     * Comprueba si el directorio de trabajo existe
     */
    public function testIfPathResultWasCreated()
    {
        $Tsa = new GenericTsa($this->valid_tsa);
        $Tsa->setPathResult('var/data/results');
        $this->assertDirectoryExists($Tsa->getPathResult());
    }
    /**
     * Testea si el archivo a sellar ha sido subido al directorio de trabajo
     */
//    public function testIfFileWasLoaded()
//    {
//
//        $Tsa = new GenericTsa($this->valid_tsa);
//        $Tsa->setPathResult('results/vivo1');
//        $Tsa->setFile('examples/data/doc1.pdf');
//
//        $this->assertFileExists('results/vivo1/'.$Tsa->getPetitionCode().'/doc1.pdf');
//    }

    /**
     * Comprueba la creacion del archivo de query
     */
    public function testTsq()
    {

        $Tsa = new GenericTsa($this->valid_tsa);
        $Tsa->setPathResult('var/results/vivo2');
        $Tsa->setFile('var/data/doc1.pdf');
        $Tsa->createTsq();
        $this->assertFileExists($Tsa->tsq_file);
    }

    /**
     * Comprueba la  creacion del archivo de response
     */
    public function testTsr()
    {

        $Tsa = new GenericTsa($this->valid_tsa);
        $Tsa->setPathResult('var/results/vivo3');
        $Tsa->setFile('var/data/doc1.pdf');
        $Tsa->createTsq();
        $Tsa->createTsr();
        $this->assertFileExists($Tsa->tsr_file);
    }

    /**
     * Comprueba si ha funionado el sellado
     */
    public function testStamp()
    {


        $Tsa = new GenericTsa($this->valid_tsa);
        $Tsa->setPathResult('var/results/vivo6');
        $Tsa->setFile('var/data/doc1.pdf');
        $this->assertEquals(true, $Tsa->stamp());
    }

    /**
     * Verifica si puede verificar un sello de tiempo
     */
    public function testVerify()
    {

        $Tsa = new GenericTsa($this->valid_tsa);
        $Tsa->setTsaDataFolder('var/data/tsa/free/');
        $Tsa->setPathResult('results/vivo6/210906_170533_77d159d2bb/');
        $Tsa->setFile('var/data/doc1.pdf');
        $Tsa->verify();
        $this->assertEquals(true, $Tsa->stamp());
    }
}
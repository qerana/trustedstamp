<?php
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Qerana\Tools;

/**
 * Utilidades para el manejo de archivos
 *
 * @author diemarc
 */
class File
{
    public

    /**
     * @var string, prefijo a utlizar para el nuevo nombre del archivo copiado
     */
        $prefix = '_cc';
    protected

    /**
     * @var string, path del archivo con q se realizara operaciones
     */
        $file_source,
        /**
         * @var string, nombre con extension del file_source
         */
        $basename,
        /**
         * @var string, nombre sin extension del file_source
         */
        $filename,
        /**
         * @var stirng, extension del file_source
         */
        $extension;

    public function __construct()
    {

    }

    /**
     * Setea un archivo
     * @param string $source
     * @throws \InvalidArgumentException
     */
    public function setFile(string $source): object
    {
        $source_path = realpath($source);

        if ($source_path === false) {
            Throw new \InvalidArgumentException('Source file not found:'.$source);
        }

        $this->file_source = $source_path;

        $this->setSourceInfo();

        return $this;
    }

    /**
     * Obtiene informacion del source file
     */
    public function setSourceInfo()
    {

        $source_info     = pathinfo($this->file_source);
        $this->basename  = $source_info['basename'];
        $this->filename  = $source_info['filename'];
        $this->extension = $source_info['extension'];
    }

    /**
     * Copia un archivo a otra ubicacion
     * @param string $destination_folder , path donde queremos copiar el archivo
     * @param bool $force , si es true reemplazar el archivo aunque existe, sino arroja exception
     * @param string $new_name , si se envia es el nuevo nombre a usar
     * @return boola
     * @throws \RuntimeException
     */
    public function copy(string $destination_folder, bool $force = false,
                         string $new_name = ''): bool
    {
        // si esta seteado el source
        $this->checkFile();

        // utilizamos el metodo estatico make de folder, para
        // que cree si no existe el destino y devuelva el path absoluto
        $destination = Folder::make($destination_folder);

        // si esta seteado el nombre de archivo utilizamos eso, sino usamos el mismo
        // con un prefiji
        $this->filename   = (empty($new_name)) ? $this->filename.$this->prefix : $new_name;
        $path_destination = $destination.'/'.$this->filename.'.'.$this->extension;

        // si force es true, aunque el destino exista va a reemplazarlo
        if ($force) {

            return copy($this->file_source, $path_destination);
        } else {
            $destination_exists = realpath($path_destination);

            // si ya existe el archivo destinos
            if ($destination_exists !== false) {
                throw new \RuntimeException('File destination already exists:'.$destination_exists.', u can force the copy with copy($destination,true)');
            } else {

                // sino copiamos
                return copy($this->file_source, $path_destination);
            }
        }
    }

    /**
     * Convierte un archivo en base_64
     * @return type array
     * @throws \InvalidArgumentException , cuando no existe el archivo a convertir
     */
    public function toBase64(): array
    {

        // checheamos si existe el fichero
        $this->checkFile();

        $handle = @fopen($this->file_source, 'r');


        if ($handle === false) {
            Throw new \RuntimeException('Error at fopen the file');
        }

        $contents = fread($handle, filesize($this->file_source));
        fclose($handle);


        return ['content' => base64_encode($contents), 'filename' => str_replace(" ",
                "_", $this->file_source)];
    }

    /**
     * Comprueba que el archivo ha sido cargado
     * @throws \InvalidArgumentException
     */
    protected function checkFile()
    {
        // si esta seteado el source
        if (empty($this->file_source)) {
            throw new \InvalidArgumentException('Source file not found, set whit $File->setFile($path)');
        }
    }
}
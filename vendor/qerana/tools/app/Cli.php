<?php
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Qerana\Tools;

/**
 * Utilidades generales para ejecucion de comandos via cli
 *
 * @author diemarc
 */
class Cli
{

    /**
     * Ejecuta un comando mediante exec
     * @param string $cmd , el comando a ejecutar
     * @param bool $retval , si es true, devolvera un array $retarray
     * @return type 
     * @throws \RuntimeException
     */
    public static function run(string $cmd, bool $retval = false)
    {

        if(empty($cmd)) {
           Throw new \InvalidArgumentException('Error: command to run is empty!!');
        }
        // array donde guardaremos la salida del comando a ejecutar
        $retarray = [];

        // codigo de retorno a utilizar
        $retcode = 0;
        exec($cmd." 2>&1", $retarray, $retcode);

        if ($retcode !== 0) {

            throw new \RuntimeException('Error at run cmd:'.$cmd.',error:'.implode(', ',
                    $retarray));
        }

        return ($retval) ? print_r($retarray, true) : true;
    }


    public static function read(){
        return 'reading';
    }
}
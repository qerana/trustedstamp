<?php

/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Qerana\Tools;

/**
 * Description of Folder
 *
 * @author diemarc
 */
class Folder {

    
    /**
     * Comprueba si existe una carpeta, si no existe lo crea, y devuelve la ruta
     * absoluta de la carpeta
     * @param string $folder , la carpetagit 
     * @param int $perm , permisos para crearlo
     * @param bool $recursive , crear recursivamente
     * @return string , el path absoluto de la carpeta
     * @throws \ErrorException
     */
    public static function make(string $folder, int $perm = 0777, bool $recursive = true) {
        // si no existe lo creamos
        if (!is_dir($folder)) {
            if (!mkdir($folder, $perm, $recursive)) {
                throw new \ErrorException('Error at create:' . $folder);
            }
        }

        return realpath($folder);
    }

}

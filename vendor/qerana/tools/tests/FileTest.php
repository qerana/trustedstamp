<?php
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Tests;

use PHPUnit\Framework\TestCase;
use Qerana\Tools\File;

/**
 * Description of FileTest
 *
 * @author diemarc
 */
class FileTest extends TestCase
{
    protected
        $valid_file    = 'var/data/test.txt',
        $invalid_file  = 'var/data/not_exists.txt',
        $readonly_file = 'var/data/test_1.txt',

        $path_result = 'var/data/result/';

    public function testIfConstructReturnsObject()
    {
        $this->assertIsObject(new File());
    }

//
//    /**
//     * tests de conversion de base 64
//     */
    public function testIfThrowExceptionIfFileNotExists64()
    {

        $this->expectException(\InvalidArgumentException::class);
        $File = new File();
        $File->setFile($this->invalid_file);
    }

    public function test64IfThrowExceptionIfFileNotSetted()
    {

        $this->expectException(\InvalidArgumentException::class);
        $File = new File();
        $File->toBase64();
    }

    public function test64SetFileReturnsObject()
    {
        $File     = new File();
        $Instance = $File->setFile($this->valid_file);
        $this->assertIsObject($Instance);
    }

//
    public function test64IfReturnArray()
    {

        $File = new File();
        $this->assertIsArray($File->setFile($this->valid_file)->toBase64());
    }

    public function test64IfContentKeyIsNotEmpty()
    {

        $File    = new File();
        $content = $File->setFile($this->valid_file)->toBase64();
        $this->assertNotEmpty($content['content']);
    }

//    public function test64IfThrowExceptionAtReadOnlyfile()
//    {
//
//        $this->expectException(\RuntimeException::class);
//        $File = new File();
//        $File->setFile($this->readonly_file);
//        $File->toBase64();
//    }

//
//
//    /**
//     * Tests de copiados
//     */
    public function testIfThrowExceptionAtCopyEmptyFileSource()
    {
        $this->expectException(\InvalidArgumentException::class);
        $File = new File();
        $File->copy('var/noexiste1', false, 'otro');
    }

    public function testIfThrowExceptionAtCopyWhereDestinationExistsAndNotForced()
    {
        $this->expectException(\RuntimeException::class);
        $File = new File();
        $File->setFile($this->valid_file);
        $File->copy('var/');
    }

    public function testCopyWhereDestinationExistsAndForced()
    {
        $File = new File();
        $File->setFile($this->valid_file);
        $this->assertTrue($File->copy('var', true));
    }

//
    public function testCopyWhenDestinationFolderDontExists()
    {

        $File = new File();
        $this->assertTrue($File->setFile($this->valid_file)->copy('var/noexiste1',
                true));
    }

//
//
    public function testCopyDiferentName()
    {

        $File = new File();
        $this->assertTrue($File->setFile($this->valid_file)->copy('var/noexiste1',
                true, 'new_name'));
    }

    
}
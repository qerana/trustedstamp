<?php
/**
 * MIT License

  Copyright (c) 2021 Diego Rojas

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

namespace Qerana\Trustedts;

use Qerana\Tools\Folder,
    Qerana\Tools\Cli;

/**
 * AbstractTsa
 *
 * Funcionalidades básicas de cualquier TSA.
 * Todas las clases TSA tienen que extender de esta
 * 
 * @author diemarc
 */
abstract class AbstractTsa
{
    public

    /** @string , archivo TSQ, el hash del archivo a sellar */
        $tsq_file = 'request.tsq',
        /** @string, archivo TSR , la respuesta del TSA  */
        $tsr_file = 'response.tsr',
        /** @string , el nombre con extension del archivo a sellar */
        $file_basename,
        /** @string , la extension del archivo a sellar */
        $file_extension,
        /** @string, el nombre solo sin la extension del archivo a sellar */
        $file_name;
    protected

    /** @string , el archivo sobre el que se realizara el sellado de tiempo */
        $file,
        /**
         * @string , almacen donde se guarda el archivo crt y pem, es decir los certificados raiz para la verificacion
         * de sellado de documentos
         */
        $tsa_data_folder = '',
        /** @string , url del servidor que  funciona como TSA */
        $tsa_url_server,
        /**
         * @bool, si el TSA necesita autentificacion
         */
        $auth            = false,
        /** @string , el usuario de las credenciales para el TSA */
        $user            = '',
        /**
         * @string , el pass de las credenciales del TSA
         */
        $pass            = '',
        /**
         *  @string, algoritmo de hash
         */
        $algo_hash       = 'sha256',
        /**
         *  @string, path donde se guardaran los resultados
         */
        $path_result,
        /**
         * @string , cadena alfanumerica que identifica a la solicitud de sellado,
         * sera utilizado como nombre de la ruta $path_result;
         */
        $petition_code;

    /**
     *      * @param string $tsa_server , url del tsa
     */
    public function __construct(string $tsa_server)
    {

        if (filter_var($tsa_server, FILTER_VALIDATE_URL) === false) {
            throw new \InvalidArgumentException('La url del servidor no es valido');
        }

        $this->tsa_url_server = $tsa_server;
    }

    /**
     * Setea el archivo sobre el q se realizara el sellado
     * @param string $file
     * @throws \InvalidArgumentException
     */
    public function setFile(string $file)
    {


        if (!is_dir($this->path_result)) {
            throw new \InvalidArgumentException('Result path no esta seteado, primero setPathResult para setearlo:');
        }

        $filepath = realpath($file);

        if (!$filepath) {
            throw new \InvalidArgumentException('El archivo a sellar no existe, o la ruta esta mal:'.$file);
        }

        $this->file = $filepath;

        // obtenemos informacion del archivo
        $path_info = pathinfo($this->file);

        $this->file_basename  = $path_info['basename'];
        $this->file_name      = $path_info['filename'];
        $this->file_extension = $path_info['extension'];
    }

    /**
     * Verifica utilizando cURL si el servidor de TSA esta respondiendo
     * @throws \RuntimeException
     */
    public function verifyTsa()
    {

        // codigos de conexiones ok
        $codes_ok = [200, 302, 403];

        $timeout      = 10;
        $ch           = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->tsa_url_server);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $http_respond = curl_exec($ch);
        $http_respond = trim(strip_tags($http_respond));
        $http_code    = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if (!in_array($http_code, $codes_ok)) {
            throw new \RuntimeException('El servidor respondio con '.$http_code.', verifique la url:'.$this->tsa_url_server);
        }
    }

    /**
     * Setea las credentiales para el servidor TSA.
     * @param string $user
     * @param string $password
     * @throws \InvalidArgumentException
     */
    public function setCredentials(string $user, string $password)
    {

        $this->auth = true;

        $this->user = trim($user);
        $this->pass = trim($password);

        if (($this->user === '') OR ($this->pass === '')) {

            throw new \InvalidArgumentException('Usuario y/o password no seteados');
        }
    }

    /**
     *  Setea el algoritmo de hash
     * @param string $algo
     * @return void
     */
    public function setAlgoHash(string $algo): void
    {
        $this->algo_hash = $algo;
    }

    /**
     * Establece la carpeta donde se guarda los archivos relacionados con el TSA
     * ej: el cert, el .pem etc, lo necesario si queremos validarlo.
     * la ruta se pone desde la raiz del proyecto
     * @param string $folder
     */
    public function setTsaDataFolder(string $folder): void
    {

        $this->tsa_data_folder = Folder::make($folder);
    }

    /**
     * Establece el directorio donde se guardaran los resultados
     * Aqui guardaremos los tsq, tsr y el propio archivo a sellar
     * @param string $path_result
     */
    public function setPathResult(string $path_result)
    {


        // asignamos la ruta completa
        $this->path_result = Folder::make($path_result);

        // ponemos la ruta completa de los archivos
        $this->tsq_file = $this->path_result.'/request.tsq';
        $this->tsr_file = $this->path_result.'/response.tsr';
    }

    /**
     * Crea el sello de tiempo
     * @return type
     */
    public function stamp()
    {


        try {

            $this->petition_code = date('ymd_His').'_'.substr(str_shuffle(MD5(microtime())),
                    0, 10);

            // formamos un nuevo path con el codigo de petition
            $path_stamp = $this->path_result.'/'.$this->petition_code;

            $this->setPathResult($path_stamp);

            // copiamos el archivo
            $this->copyFile();

            // creamos el archivo de peticion de sellado
            $this->createTsq();

            // sellamos con el TSA y generamos el TSR
            return $this->createTsr();
        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex);
            die();
        }
    }

    /**
     * Crea el archivo de peticion "request.tsq" (timestamp query ) 
     * Es un hash del archivo a sellar, utiliza comandos via exec
     * @throws \Exception 
     */
    public function createTsq()
    {

        try {
            $cmd = 'openssl ts -query -data '.$this->file.' -no_nonce -'.$this->algo_hash.' -out '.$this->tsq_file;
            Cli::run($cmd);

            if (!is_file($this->tsq_file)) {
                throw new \RuntimeException('Error al crear el TSQ');
            } else {
                return true;
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * Obtiene el sello de tiempo enviando el archivo TSQ al TSA (TimeStampAuthority)
     */
    public function createTsr()
    {

        // solo si el tsq esta generado en el dir de trabajo

        if (is_file($this->tsq_file)) {
            $cmd = "curl -H 'Content-Type:application/timestamp-query' --data-binary @".$this->tsq_file.' '.$this->tsa_url_server." -o ".$this->tsr_file;

            // si necesita auth
            if ($this->auth) {


                $cmd .= "  -u ".$this->user.":".$this->pass;
            }

            Cli::run($cmd);

            // si no esta creado
            if (!is_file($this->tsr_file)) {
                throw new \Exception('Error al crear el TSR');
            } else {
                return true;
            }
        } else {
            throw new \Exception('El archivo TSQ no existe, no se puede enviar al TSA para el sellado');
        }
    }

    /**
     * verifica un documento si ha sido alterado
     * @param string $file
     * @param string $petition
     */
    public function verify()
    {

        $cacrt  = realpath($this->tsa_data_folder.'/tsa.crt');
        $cafile = realpath($this->tsa_data_folder.'/cacert.pem');

        // verificamos si existen los archivos necesarios para su verificacion
        if (!$cacrt OR!$cafile) {
            throw new \InvalidArgumentException('No existen los archivos de verificacion (.crt, .pem) en '.$this->tsa_data_folder);
        }

        $cmd = 'openssl ts -verify -data '.$this->file.' -in '.$this->tsr_file.' -CAfile '.$cafile.' -untrusted '.$cacrt;

        echo Cli::run($cmd, true);
    }

    /**
     * Copia el archivo a sellar en su directorio de trabajo
     * @throws \Exception
     */
    protected function copyFile()
    {

        // la ubicacion donde queremos copiar
        $destination_path = $this->path_result.'/'.$this->file_basename;


        if (copy($this->file, $destination_path)) {

            // asignamos el nuevo path del arhivo a trabajar
            $this->file = $destination_path;
        } else {
            throw new \Exception('Error al copiar el archivo a sellar a:'.$destination_path);
        }
    }

    /**
     * Devuelve la ruta completa de datos del tsa
     * @return string
     */
    public function getTsaFolder(): string
    {
        return $this->tsa_data_folder;
    }

    /**
     * Obtiene la url del TSA
     * @return string
     */
    public function getTsaUrl(): string
    {
        return $this->tsa_url_server;
    }

    /**
     * Obtiene el usuario del tsa
     * @return type
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * Obtiene el password del usuario del tsa
     * @return type
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    public function getAuth(): bool
    {
        return $this->auth;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function getPathResult(): string
    {
        return $this->path_result;
    }

    public function getPetitionCode(): string
    {
        return $this->petition_code;
    }
}